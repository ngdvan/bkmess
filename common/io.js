let socket_io = require('socket.io');


let io = socket_io();
let socketAPI = {};
let userList= [{
    id: 0,
    username: "BKBot",
}];

//Your socket logic here
socketAPI.io = io;
// new added comment
io.on('connection', async (socket)=>{
    let savedCommand = []
    console.log("connected! id: ", socket.id)
    // Event: new user registers and connects
    socket.on('signin', username => {
        userList.push({
            id: socket.id,
            username: username,
        })
        // broadcast new user to everyone
        io.emit('new user',{
            id: socket.id,
            username: username
        })
    })
    // Event: client disconnects with server
    socket.on('disconnect', function(){
        for(let i = 0 ; i < userList.length; i++){
            if(userList[i].id == socket.id){
                io.emit('disconnect user', userList[i])
                userList.splice(i);
                break;
            }
        }
    })
    

    // Event: new user sends a message to server
    socket.on('message', function (data){
        let {id, message, senderID} = data;
<<<<<<< HEAD
        let {id1, message1, senderID1} = data;
=======
        let {id, message, senderID} = data;
>>>>>>> ff391867ebbc9e4bd041a9d0c75eb04aeebc432f
        console.log("message from: ", senderID, " to: ", id, " content:", message)
        // if not a message to bot:
        if(id != 0){
            for(let i = 0; i < userList.length; i ++){
                if(userList[i].id == id){
                    io.to(id).emit('coming message', {message, senderID});
                    break;
                }
            }
        }
        
    })
})

io.on('disconnect', async (socket)=>{

})

module.exports= {
    socketAPI,
    userList
}
