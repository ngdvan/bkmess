$(document).ready(function () {
    let socket = io();
    socket.emit('signin', username)
    let selectedUserID = 0;

    function addNewContact(user){
        if(user.id == socket.id)  {
            console.log('same id')
            return;
        }
        $('#contacts').append(`
            <li class="list-group-item-action" id = "contact-${user.id}">
                <div class="d-flex bd-highlight">
                    <div class="img_cont">
                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img">
                        <span class="online_icon"></span>
                    </div>
                    <div class="user_info">
                        <span>${user.username}</span>
                        <p>${user.username} is online</p>
                    </div>
                </div>
            </li>
        `)
    }
    
    function disconnectContact(user){
        let contact = $(`#contact-${user.id}`)
        if(contact)
            contact.remove();
    }
    
    function formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    // button, page events
    // on send message button
    $('#send-btn').click(function(e){
        
        // messsage
        let message = $('#message-txt').val();
        $('#message-txt').val('');
        if(message){
            $('#message-body').append(`
            <div class="d-flex justify-content-end mb-4">
                <div class="msg_cotainer_send">
                    ${message}
                    <span class="msg_time_send"> ${formatAMPM(new Date)}</span>
                </div>
                <div class="img_cont_msg">
                    <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                </div>
            </div>
            `)

            // hien menu tu bot

        }
        // send message
        socket.emit('message', {message, id:selectedUserID, senderID: socket.id})
    })

    // on contact selected button
    $(document).on('click','[id^=contact-]', function(e){
        let obj = $(e.target).closest('li')[0]
        selectedUserID = obj.id.slice(8)
        
        $('#message-body').empty();
        $(`li`).not(`#contact-${selectedUserID}`).removeClass('active')
        $(`#contact-${selectedUserID}`).addClass('active')

        if(selectedUserID == 0){
            $('#message-body').append(`
                <div class="d-flex justify-content-start mb-4">
                    <div class="img_cont_msg">
                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                    </div>
                    <div class="msg_cotainer">
                        Hi, how are you ${username}?
                        <span class="msg_time">8:40 AM, Today</span>
                    </div>
                </div>
            `)
        }
    })


    // socket io logics
    socket.on('new user', user => {
        addNewContact(user)
    })

    socket.on('disconnect user', user => {
        disconnectContact(user)
        console.log("disconnected!")
    })

    socket.on('coming message', data =>{
        let {message, senderID} = data;
        if(message){
            if($(`#contact-${senderID}`).hasClass('active')){
                $('#message-body').append(`
                <div class="d-flex justify-content-start mb-4">
                    <div class="img_cont_msg">
                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                    </div>
                    <div class="msg_cotainer">
                        ${message}
                        <span class="msg_time">${formatAMPM(new Date)}</span>
                    </div>
                </div>
                `)
            }
            else{
                selectedUserID = senderID;
                $('#message-body').empty();
                $(`li`).not(`#contact-${senderID}`).removeClass('active')
                $(`#contact-${senderID}`).addClass('active')
                $('#message-body').append(`
                <div class="d-flex justify-content-start mb-4">
                    <div class="img_cont_msg">
                        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
                    </div>
                    <div class="msg_cotainer">
                        ${message}
                        <span class="msg_time">${formatAMPM(new Date)}</span>
                    </div>
                </div>
                `)
                
            }
        }
    })
});